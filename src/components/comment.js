import React, { Component } from 'react';
import './comment.css';

//Comment Component
class Comment extends Component{
	constructor(props){
    super(props);
    this.state = {
      commentId: this.props.comment.id,
      postId: this.props.comment.postId,
      name: this.props.comment.name,
      email: this.props.comment.email,
      body: this.props.comment.body,
      comments: [],
      isShow: false,
    };
  }

	hide = () =>{
	  	let v = this.state.isShow;
		this.setState({
			isShow: !v
		})
	}
	
	editPost = () =>{
		
		const commId = this.state.commentId;
		const postId = this.state.postId;
		const name = this.state.name;
		const email = this.state.email;
		const body = this.refs.edit.value;
		// console.log("check", commId, postId);
		fetch(`https://jsonplaceholder.typicode.com/comments/${commId}`, {
		    method: 'PUT',
		    body: JSON.stringify({
		      id: commId,
		      name: name,
		      email: email,
		      postId: postId
		    }),
		    headers: {
		      "Content-type": "application/json; charset=UTF-8"
		    }
		  	})
		  	.then(response => response.json())
		  	.then(json => {
		  		console.log(json);
		  		alert("Edited");
		  		this.setState({
		  			isShow: false
		  		})
		  	})

		this.refs.edit.value = null;
	}

	delPost = () => {
		const commId = this.state.commentId;
		fetch(`https://jsonplaceholder.typicode.com/comments/${commId}`, {
		  method: 'DELETE'
		})
		.then(response => response.json())
	  	.then(json => {
	  		console.log(json);
	  		alert("Deleted");
	  	})
	}

	render(){
		// console.log("comm", this.state);
		const name = this.props.comment.name;
		const email = this.props.comment.email;
		const body = this.props.comment.body;
		// console.log("key", this.props.key);
		return(
			<div className="comment">
				<div className="comment-title">
					{name}<span>({email})</span>
				</div>
				<div className="comment-body">
					{body}
					{this.state.isShow ?
						<div className="form-group">
						<label htmlFor="edit">Edit:</label>
					  	<textarea ref="edit" className="form-control" rows="2" cols="2" id={this.state.postId}  placeholder="Edit Your Comment..."></textarea>
					  	<button className="submit btn-primary" type="submit" onClick={this.editPost}>Submit</button>
					  	</div>
					  	: null
					}
				</div>
				<button className="edit-post" onClick={this.hide}>Edit</button>
				<button className="delete-post btn-danger" onClick={this.delPost}>delete</button>
			</div>
		);
	}
}

export default Comment;
import React, { Component } from 'react';
import './post.css';
import Comment from './comment';

//Post Component
class Post extends Component{
	constructor(props){
    super(props);
    this.state = {
      postId: this.props.post.id,
      userId: this.props.post.userId,
      isShow: false,
      comments: []
    };
  }

  componentDidMount(){
	const url = `https://jsonplaceholder.typicode.com/comments?postId=${this.state.postId}`;

	fetch(url).then(response => response.json())
		.then((data) => {
			this.setState({
				comments: data
			})
			// console.log("data",data);
		});
  }

  hide = () =>{
  	let v = this.state.isShow;
	this.setState({
		isShow: !v
	})
  }

  submit = () =>{
  	console.log("klik", this.state.postId);
  	const name = this.refs.name.value;
  	const email = this.refs.email.value;
  	const msg = this.refs.body.value;
  	const postId = this.state.postId;

  	fetch('https://jsonplaceholder.typicode.com/comments', {
    method: 'POST',
    body: JSON.stringify({
      	name: name,
      	email: email,
      	body: msg,
      	postId: postId
    }),
    headers: {
      "Content-type": "application/json; charset=UTF-8"
    }
  	})
 	.then(response => response.json())
  	.then(json => {
  		console.log("post",json);
  		alert("Name :  " +json.name+" \nEmail  : "+json.email+"\nDone");
  	})

  	this.refs.name.value = null;
  	this.refs.email.value = null;
  	this.refs.body.value = null;
  }

	render(){
		// console.log("work",this.state);
		const title = this.props.post.title;
		const body = this.props.post.body;
		// console.log("postid", this.state.postId);
		return(
			<div className="post">
				<div className="post-title">
					{title}
				</div>
				<div className="post-body">
					{body}
				</div>
				
				<div className="form-group">
					<label htmlFor="name">Name : </label>
					<input ref="name" className="form-control" type="text" name="name" placeholder="Name" /><br/>
					<label htmlFor="email">Email :  </label>
					<input ref="email" className="form-control" type="text" name="email" placeholder="Email" /><br/>
				  	<label htmlFor="comment">Comment:</label>
				  	<textarea ref="body" className="form-control" rows="2" cols="2" id={this.state.postId}  placeholder="Please be nice..."></textarea>
				  	<button className="submit btn-primary" type="submit" onClick={this.submit}>Submit</button>
				</div>
				<button className="more-button align-self-end" onClick={this.hide}>...</button>	
				{this.state.isShow ? 
				<div className = "comment">
				 	{this.state.comments.map((comment)=>
			          <Comment key={comment.id} comment={comment}/>
			        )}
				</div>
				: null
				}
			</div>
		);
	}
}

export default Post;

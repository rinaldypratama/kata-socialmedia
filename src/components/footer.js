import React, { Component } from 'react';
import './footer.css';

//Footer Component
class Footer extends Component{

	render(){
		return(
			<footer className="footer">
			    <div className="container">
			        <span className="text-muted">KATA Social &#169; {new Date().getFullYear()}</span>
			    </div>
			</footer>
		)
	}
}

export default Footer;



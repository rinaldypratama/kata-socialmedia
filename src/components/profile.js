import React, { Component } from 'react';
import './profile.css';

import Avatar from './avatar';
import Album from './album';
import Post from './post';

//Component to display user profile
class Profile extends Component{
	constructor(props){
    super(props);
    this.state = {
      album: [],
      user: "",
      uid: null,
      posts: [],
      username: ""
    };
    
  }

	componentDidMount(){

  }
  getUser(data){
	// console.log(this.state);
  	if(this.state.username === data)return;
	fetch(`https://jsonplaceholder.typicode.com/users?username=${data}`)
	.then((response) => response.json())
	.then((user) =>{
		user.map((u) =>
			this.setState({
				user: u,
				uid: u.id,
				username: data
			})
		)
	this.getData(this.state.uid);
	});
  }

  getData(data){
    const apiAlbum = fetch(`https://jsonplaceholder.typicode.com/albums?userId=${data}`).then((response) => response.json()
    );
    const apiPost = fetch(`https://jsonplaceholder.typicode.com/posts?userId=${data}`).then((response) => response.json()
    );
    Promise.all([apiAlbum,apiPost]).then((values) =>{
        this.setState({
          album: values[0],
          posts: values[1]
        });
  	// console.log(this.state.uid, this.state.posts);
    })
  }

	render(){
		const params = this.props.match.params;
        this.getUser(params.level);
        // console.log("tes", this.props.match);
		return(
			<div className="profile">
				<div className="user-detail">
				<div>	
					<Avatar key={this.state.uid} user={this.state.user}/>
					<p>{this.state.user.name}</p>
					<p>{this.state.user.email}</p>
					<p>{this.state.user.website}</p>
				</div>
				</div>
				<div className="user-album">
					<h5>{this.state.username}'s Album</h5>
					{this.state.album.map((a)=>
						<Album key={a.id} album={a} username={params.level}/>
						// console.log("album?", a)
					
					)}
				</div>
				<div className="user-post">
					<h5>{this.state.username}'s Posts</h5>				
					{this.state.posts.map((post)=> 

                      <Post key={post.id} post={post}/>
                      // console.log("masuk?", post);
					
					
                    )}
				</div>
			</div>
		)
	}
}

export default Profile;

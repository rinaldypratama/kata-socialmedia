import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import './header.css';

//Header Component
class Header extends Component{
  
  render(){
    return(
      <div className="header">
        
          
          <nav className="navbar navbar-expand-md navbar-dark bg-dark">
            <NavLink className="navbar-brand" to="/">KATA<sup><i className="ss fa fa-bars"></i></sup></NavLink>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>

            <div className="collapse navbar-collapse" id="navbarsExampleDefault">
              <ul className="navbar-nav mr-auto">
                <li className="nav-item active">
                  <NavLink className="nav-link" to="/home">Home <span className="sr-only">(current)</span></NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/Profile">Profile</NavLink>
                </li>
              </ul>
            </div>
          </nav>
        
      </div>
      
    );
  }
}

export default Header;
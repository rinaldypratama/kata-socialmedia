import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import blankProfilePicture from '../img/blankProfilePicture.png';
import './avatar.css';

//Component for display avatar and username
class Avatar extends Component{
	render(){
		// console.log(this.props)
		const username = this.props.user.username;
		const urlImg =  this.props.user.imageUrl || blankProfilePicture;

		const style = {
			backgroundImage: `url('${urlImg}')`
		};

		return(
			<Link to={`/profile/${username}`} className="active" user={this.props} id={this.props.user.id}>
				<div className="wrapper">
					<div className="ava-img" style={style}></div>
					<div className="ava-name">
					{username}
					</div>
				</div>
			</Link>
			);

	}
}

export default Avatar;
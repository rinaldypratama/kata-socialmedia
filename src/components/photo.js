import React, { Component } from 'react';
import './photo.css';

//Component for display all picture
class Photo extends Component{
	constructor(props){
		super(props);
		this.state = {
			photos: [],
			title:[],
			albumId: null,
		};
	}

	componentDidMount(){
		let v = this.props.location.pathname ;
		let z = v.split("/").pop();
		const url = `https://jsonplaceholder.typicode.com/photos?albumId=${z}`;
		fetch(url).then((response) => response.json())
		.then((data) => 
			this.setState({
				photos:data,
				title:data.title,
				albumId:z
			})
		 	)
	}




	render(){
		return(
			<div className="photo">
			 	<div className="photo-source">
			 		{this.state.photos.map((data)=>
			 				
					<img className="border-photo" key={data.id} src={data.thumbnailUrl}/>

			 		)}
			 	</div>
			</div>
		)
	};
}

export default Photo
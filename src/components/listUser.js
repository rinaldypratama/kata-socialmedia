import React, { Component } from 'react';
import './listUser.css';
import Avatar from'./avatar';

//Sidebar Component
class ListUser extends Component{
	constructor(props){
    	super(props);
		this.state = {
			listUsers: []
		};
	}


	render(){
		return(
			<div className="userBox">
				<h5>Another User</h5>
				{this.props.users.map((user)=> 
			          <Avatar key={user.id} user={user}/>
			    )}
			</div>
		)
	};
}

export default ListUser;
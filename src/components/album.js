import React, {Component} from 'react';
import './album.css';
import { Link } from 'react-router-dom';
import blankProfilePicture from '../img/blankProfilePicture.png';

//Component for display album and album title
class Album extends Component{
	constructor(props){
		super(props);
		this.state={
			albumId:this.props.album.id,
			uid:this.props.album.userId,
			username:this.props.username,
			photos:[]
		}
	}
	
	componentDidMount(){
	const url = `https://jsonplaceholder.typicode.com/Photos?albumId=${this.state.uid}`;

	fetch(url).then(response => response.json())
		.then((data) => {
			this.setState({
				photos: data
			})
			// console.log("Photo", this.props);
		});
  }
	render(){
		// console.log("album kenangan", this.props.photos[this.props.album.id]);
		const titleAlbum = this.props.album.title;
		const idAlbum = this.props.album.id;
		const userId = this.props.album.userId;
		const urlImg =  blankProfilePicture;

		const style = {
			backgroundImage: `url('${urlImg}')`
		};

		return(
			<Link to={`/profile/${this.props.username}/album/${this.state.albumId}`} className="active"  id={this.state.albumId}>
				<div className="album">
					<div className="album-img" style={style}></div>
					<div className="album-name">
					{titleAlbum}
					</div>
				</div>
			</Link>
		)
	}
}

export default Album;
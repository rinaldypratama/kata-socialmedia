import React, { Component } from 'react';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import './App.css';

import Avatar from './components/avatar';
import Post from './components/post';
import Header from './components/header';
import Footer from './components/footer';
import ListUser from './components/listUser';
import Home from './components/home';
import Profile from './components/profile';
import Photo from './components/photo';
import Auth from './components/auth';


//Main Component
class App extends Component {
  constructor(props){
    // console.log("apap props",props);
    super(props);
    this.state = {
      users: [],
      uid: "",
      posts: []
    };
  }

  //get data from api
  componentDidMount(){
    const apiUser = fetch('https://jsonplaceholder.typicode.com/users').then((response) => response.json()
    );
    const apiPost = fetch('https://jsonplaceholder.typicode.com/posts').then((response) => response.json()
    );
    Promise.all([apiUser,apiPost]).then((values) =>{
        this.setState({
          users: values[0],
          posts: values[1]
        });
        // console.log(this.state.posts);
    })
  }

  render() {
    return (
      <Router>
         <div>
            <Header key="header"/> 
            <div className="content">
              <div className="main">

              <Switch>
                <Route exact path="/" component={Home} exact={true}/>
                <Route path="/home" component={Home}/>
                <Route exact path="/profile" component={Auth}/>
                <Route exact path="/profile/:level" component={Profile}/>
                <Route  path="/profile/:level/:level" component={Photo}/>
              </Switch>
              </div>
                <div className="user-list">
                  <ListUser key="sidebar" users={this.state.users}/>
                </div>
            </div>
            <Footer key="footer"/>      
         </div>
      </Router>
    );
  }
}

export default App;
              